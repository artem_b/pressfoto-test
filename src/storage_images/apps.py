from django.apps import AppConfig


class StorageImagesConfig(AppConfig):
    name = 'storage_images'
