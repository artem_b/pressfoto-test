from django import forms
from django.db.models import Q
from .models import Image


class DateInput(forms.DateInput):
    input_type = 'date'


class FormWrappMixin(object):
    def as_div(self):
        return self._html_output(
            normal_row='<div class="form-item" %(html_class_attr)s>%(label)s %(field)s %(help_text)s %(errors)s</div>',
            error_row='<div class="error">%s</div>',
            row_ender='</div>',
            help_text_html='<div class="hefp-text">%s</div>',
            errors_on_separate_row=False)


class ImageCreateForm(FormWrappMixin, forms.ModelForm):
    class Meta:
        model = Image
        fields = ['name', 'description', 'file']


class ImageUpdateForm(FormWrappMixin, forms.ModelForm):

    class Meta:
        model = Image
        fields = ['name', 'description', 'pub_date']
        widgets = {
            'pub_date': DateInput(),
        }


class SearchForm(FormWrappMixin, forms.Form):
    q = forms.CharField(
        min_length=3,
        label='',
        required=False,
        widget=forms.TextInput(attrs={'placeholder': "Поиск"}),
    )

    def get_result(self, queryset):
        q = self.cleaned_data.get('q')
        if q:
            queryset = queryset.filter(
                Q(name__icontains=q) |
                Q(description__icontains=q)
            )
            return queryset
        return queryset
