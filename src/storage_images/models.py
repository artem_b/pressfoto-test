from datetime import datetime
from django.db import models
from core.utils.path import get_uuid_upload_path
from core.utils.signals import update_files, delete_files
from django.db.models.signals import pre_save, pre_delete


class Image(models.Model):
    name = models.CharField(
        max_length=200,
        verbose_name='название',
    )
    description = models.TextField(
        verbose_name='описание',
    )
    file = models.ImageField(
        upload_to=get_uuid_upload_path,
        verbose_name='изображение',
    )
    pub_date = models.DateField(
        verbose_name='дата съёмки',
        default=datetime.now,
    )
    update_date = models.DateTimeField(
        verbose_name='дата обновления',
        auto_now_add=True,
    )

    class Meta:
        verbose_name = 'Фотография'
        verbose_name_plural = 'Фотографии'

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('image_detail', kwargs={'pk': self.pk})


pre_save.connect(update_files, Image)
pre_delete.connect(delete_files, Image)
