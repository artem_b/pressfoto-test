from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)
from django.urls import reverse_lazy
from .models import Image
from .forms import ImageCreateForm, ImageUpdateForm, SearchForm


class ImageListView(ListView):
    model = Image
    form_class = SearchForm

    def get_queryset(self):
        queryset = super(ImageListView, self).get_queryset()
        queryset = self.get_result(queryset)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(ImageListView, self).get_context_data(**kwargs)
        context['search_form'] = self.get_form()
        return context

    def get_form(self):
        try:
            form = self._form
        except AttributeError:
            form = self.form_class(
                self.request.GET,
            )
            self._form = form
        return form

    def get_result(self, queryset):
        form = self.get_form()
        form.full_clean()
        queryset = form.get_result(queryset)
        return queryset


class ImageDetailView(DetailView):
    model = Image


class ImageCreateView(CreateView):
    template_name = 'storage_images/image_create.html'
    form_class = ImageCreateForm
    model = Image


class ImageUpdateView(UpdateView):
    template_name = 'storage_images/image_update.html'
    form_class = ImageUpdateForm
    model = Image


class ImageDelete(DeleteView):
    model = Image
    success_url = reverse_lazy('image_list')
