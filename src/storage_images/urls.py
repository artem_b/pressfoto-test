from django.conf.urls import url
from .views import (
    ImageListView,
    ImageDetailView,
    ImageCreateView,
    ImageUpdateView,
    ImageDelete
)

urlpatterns = [
    url(
        r'^(?P<pk>\d+)$',
        ImageDetailView.as_view(),
        name='image_detail',
    ),
    url(
        r'^(?P<pk>\d+)/update$',
        ImageUpdateView.as_view(),
        name='image_update',
    ),
    url(
        r'^(?P<pk>\d+)/delete$',
        ImageDelete.as_view(),
        name='image_delete',
    ),
    url(
        r'^create/$',
        ImageCreateView.as_view(),
        name='image_create',
    ),
    url(
        r'^$',
        ImageListView.as_view(),
        name='image_list',
    ),
]
