import os
import uuid


def get_uuid_upload_path(instance, filename, upload_to=None):
    '''
    Returns upload path with UUID4 hex as filename
    '''
    name, ext = os.path.splitext(filename)
    name = uuid.uuid4().hex

    filename = os.path.basename(u'%s%s' % (name, ext))

    if upload_to:
        return os.path.join(upload_to, filename)
    else:
        return filename
