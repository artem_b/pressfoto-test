from django.conf import settings
from django.db.models import FileField, ImageField


FILE_FIELD_TYPES = getattr(settings, 'FILE_FIELD_TYPES', [
    FileField, ImageField,
])


def _file_cleanup(sender, instance, is_delete_signal=False, **kwargs):
    if kwargs.get('raw'):
        return

    def delete_old_file(instance, file_name):
        if instance.pk:
            old_instance = sender.objects.get(pk=instance.pk)
            old_file_name = getattr(old_instance, field.name)
            if old_file_name:
                if file_name != old_file_name:
                    try:
                        field.storage.delete(old_file_name)
                    except:
                        pass

    for field in instance._meta.fields:
        if type(field) in FILE_FIELD_TYPES:
            file_name = getattr(instance, field.name)
            if file_name:
                if is_delete_signal:
                    try:
                        field.storage.delete(file_name)
                    except:
                        pass
                else:
                    delete_old_file(instance, file_name)
            else:
                delete_old_file(instance, file_name)


def update_files(sender, instance, **kwargs):
    _file_cleanup(sender, instance, **kwargs)


def delete_files(sender, instance, **kwargs):
    _file_cleanup(sender, instance, is_delete_signal=True, **kwargs)
