# PressFoto-test #

Тестовое задание на вакансию Python разработчика

### Как запустить проект? ###
```sh
$ git clone git@bitbucket.org:artem_b/pressfoto-test.git
$ cd pressfoto-test
$ python3 -m venv env

если Windows
$ env\Scripts\activate

если Linux или OS X
$ . env/bin/activate
$ pip install -r requirements.txt
$ cd src
$ python manage.py migrate
$ manage.py runserver  --settings=core.local_setting
```
